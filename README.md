# envgenerate

CLI-утилита для генерации файла .env на основе файла .env.example и системных переменных окружения.

## Установка

Вы можете установить `envgenerate` через [Composer](https://getcomposer.org/).

```bash
composer require qqs/envgenerate
```
### Основное предназначение
Развертывание через gitlab ci, с переменными окружения из дэшборда.

### Использование
Вы можете использовать envgenerate из командной строки.
```bash
vendor/bin/envgenerate <target.env> <source.env>
```
<target.env> - путь к файлу .env, который будет создан на основе файла .env.example и системных переменных окружения.

<source.env> - путь к файлу .env.example, который будет использоваться в качестве шаблона для генерации файла .env.

### Пример использования
Допустим, у вас есть файл .env.example со следующим содержимым:
```
DB_HOST=localhost
DB_NAME=mydatabase
DB_USER=myusername
DB_PASS=mypassword
```
Чтобы создать файл .env на основе .env.example и системных переменных окружения, выполните следующую команду:
```bash
vendor/bin/envgenerate .env .env.example
```
Если вы установили системные переменные окружения DB_HOST, DB_NAME, DB_USER и DB_PASS, то значения переменных будут использованы вместо значений из файла .env.example.

### Лицензия
Инструмент доступен на условиях лицензии MIT. Подробную информацию смотрите в файле LICENSE.
